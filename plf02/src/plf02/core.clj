(ns plf02.core)

(defn funcion-associative?-1
  [xs]
  (associative? xs))

(defn funcion-associative?-2
  [edades]
  (associative? edades))

(defn funcion-associative?-3
  [a b]
  (let [noma a nomb b]
    (and (associative? noma) (associative? nomb))))

(funcion-associative?-1 "luis")
(funcion-associative?-2 [:luis 23 :juan 22 :martha 23 :alejandra 23])
(funcion-associative?-3 #{1 2} [1 2])

(defn funcion-boolean?-1
  [xs ys]
  (and (boolean? xs) (boolean? ys)))

(defn funcion-boolean?-2
  [xs]
  (apply boolean? xs))

(defn funcion-boolean?-3
  [xs ys zs]
  (or (boolean? xs) (boolean? ys) (boolean? zs)))

(funcion-boolean?-1 "cadena" [22])
(funcion-boolean?-2 [true])
(funcion-boolean?-3 false false 1)

(defn funcion-char?-1
  [a]
  (char? a))

(defn  funcion-char?-2
  [a b]
  (char? (concat (first a) (first b))))

(defn funcion-char?-3
  [a b c]
  (let [noma a nomb b nomc c]
    (and (char? noma) (char? nomb) (char? nomc))))

(funcion-char?-1 \w)
(funcion-char?-2 [\t \y] [\w \e])
(funcion-char?-3 \o \w \q)

(defn funcion-coll?-1
  [xs]
  (coll? xs))

(defn  funcion-coll?-2
  [a b]
  (coll? (concat a b)))

(defn funcion-coll?-3
  [e1 e2 e3]
  (or (coll? e1) (coll? e2) (coll? e3)))

(funcion-coll?-1 [\f])
(funcion-coll?-2 "si" "si")
(funcion-coll?-3 true false false)

(defn funcion-decimal?-1
  [e]
  (decimal? e)
)

(defn  funcion-decimal?-2
  [e1 e2]
  (decimal? (concat (first e1) (first e2))
))

(defn funcion-decimal?-3
  [a b c]
  (let [elea a eleb b elec c]
    (and (decimal? elea) (decimal? eleb) (decimal? elec))
))

(funcion-decimal?-1 7M) 
(funcion-decimal?-2 [9M] [2M])
(funcion-decimal?-3 99M 7M 2M)

